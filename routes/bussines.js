'use sctrict'

var express = require('express');
var router = express.Router();
var md_auth = require('../middlewares/authenticate');
var bussinesController = require('../controllers/bussines');

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: './upload/bussines'});

/* User routes */
router.post('/save-bussines/',[md_auth.ensureAuth,md_upload],bussinesController.saveBussines);
router.get('/get-bussines/',md_auth.ensureAuth,bussinesController.getBussines);
router.get('/list-bussines/:id&:page',md_auth.ensureAuth,bussinesController.listBussines);
router.put('/update-bussines/:id',[md_auth.ensureAuth,md_upload],bussinesController.updateBussines);
router.delete('/delete-bussines/:id',md_auth.ensureAuth,bussinesController.deleteBussines);
router.get('/get-image-bussines/:imageFile',bussinesController.getImageBussines);

module.exports = router;
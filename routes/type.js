'use sctrict'

var express = require('express');
var router = express.Router();
var md_auth = require('../middlewares/authenticate');
var typeController = require('../controllers/type');

/* User routes */
router.post('/save-type',md_auth.ensureAuth,typeController.saveType);
router.get('/get-type/:id',md_auth.ensureAuth,typeController.getType);
router.get('/list-type',md_auth.ensureAuth,typeController.listTypes);
router.put('/update-type/:id',md_auth.ensureAuth,typeController.updateType);
router.delete('/delete-type/:id',md_auth.ensureAuth,typeController.deleteType);

module.exports = router;
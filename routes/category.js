'use sctrict'

var express = require('express');
var router = express.Router();
var md_auth = require('../middlewares/authenticate');
var categoryController = require('../controllers/category');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './upload/category' });

/* User routes */
router.post('/save-category', [md_auth.ensureAuth, md_upload], categoryController.saveCategory);
router.get('/get-category', md_auth.ensureAuth, categoryController.getCategory);
router.put('/update-category/:id', [md_auth.ensureAuth, md_upload], categoryController.updateCategory);
router.delete('/delete-category/:id', md_auth.ensureAuth, categoryController.deleteCategory);
router.get('/get-image-category/:imageFile', categoryController.getImageCategory);
router.get('/list-categories', md_auth.ensureAuth, categoryController.listCategories);


module.exports = router;
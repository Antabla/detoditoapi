'use sctrict'

var express = require('express');
var router = express.Router();
var md_auth = require('../middlewares/authenticate');
var userController = require('../controllers/user');

/* User routes comentario */
router.get('/get-user/:id', md_auth.ensureAuth, userController.getUser);
router.post('/save-user', userController.saveUser);
router.post('/login-user', userController.loginUser);
router.get('/confirmation-email/:token', userController.confirmateEmail);
router.put('/update-user/:id', md_auth.ensureAuth, userController.updateUser);
router.delete('/delete-user/:id', md_auth.ensureAuth, userController.removeUser);

module.exports = router;

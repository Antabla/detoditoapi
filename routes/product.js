'use sctrict'

var express = require('express');
var router = express.Router();
var md_auth = require('../middlewares/authenticate');
var productController = require('../controllers/product');

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: './upload/product'});

/* User routes */
router.post('/save-product',[md_auth.ensureAuth,md_upload],productController.saveProduct);
router.get('/get-product/:id', md_auth.ensureAuth,productController.getProduct);
router.get('/list-product',md_auth.ensureAuth,productController.listProducts);
router.put('/update-product/:id',[md_auth.ensureAuth,md_upload],productController.updateProduct);
router.delete('/delete-product/:id',md_auth.ensureAuth,productController.deleteProduct);
router.get('/get-image-product/:imageFile',productController.getImageProduct);


module.exports = router;
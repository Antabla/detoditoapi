'use sctrict'

var express = require('express');
var router = express.Router();
var md_auth = require('../middlewares/authenticate');
var orderController = require('../controllers/order');

/* User routes */
router.post('/make-order',md_auth.ensureAuth,orderController.makeOrder);
router.get('/list-orders/:id',md_auth.ensureAuth,orderController.listOrders);
router.put('/update-order/:id',md_auth.ensureAuth,orderController.updateOrder);
router.get('/confirmate-order/:id&:confirmation',md_auth.ensureAuth,orderController.confirmateOrder);
router.delete('/delete-order/:id',md_auth.ensureAuth,orderController.deleteOrder);

module.exports = router;
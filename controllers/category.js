
var Cryptr = require('cryptr');
var CRYPT_KEY = "90Fd6409B";
var cryptr = new Cryptr(CRYPT_KEY);

var Category = require('../models/category');


//<--------------------- PRIVATE FUNCTIONS ------------------------------>//

//This function response a Json object with a state code HTTP, a message, a error, a some data
function responseJson(code, message, error, data) {
    return {
        code: code,
        message: message,
        error: error,
        data: data
    };
}

//this function returns the name of a image file
function uploadImageCategory(req) {
    var file_name = "null";

    if (req.files) {
        var file_path = req.files.image.path;
        var file_split = file_path.split("\\");

        file_name = file_split[2];
    }

    return file_name;
}

//<--------------------- PUBLIC FUNCTIONS ------------------------------>//

//save a user
function saveCategory(req, res) {
    var params = req.body;

    var category = new Category();

    category.name = params.name;
    category.image = uploadImageCategory(req);

    category.save((err, categoryStorage) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!category) {
                res.json(responseJson(404, 'No se pudo guardar la categoria', true, categoryStorage));
            } else {
                res.json(responseJson(200, 'Categoria guardada', false, categoryStorage));
            }
        }
    });
}

function getCategory(req, res) {
    var categoryId = req.params.id;

    Category.findById(categoryId, (err, category) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (condition) {
                res.json(responseJson(404, 'No se encontro ninguna categoria', true, categoryId));
            } else {
                res.json(responseJson(200, 'Categoria encontrada', false, category));
            }
        }
    });
}

//get a categories
function listCategories(req, res) {
    var page = req.params.page;
    var itemsPerPage = 6;

    Category.find().sort().paginate(page, itemsPerPage, (err, category, total) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!category) {
                res.json(responseJson(404, 'Error al obtener las categorias', true, {}));
            } else {
                res.json(responseJson(200, 'Categorias obtenidas', false, { total: total, category: category }));
            }
        }
    });

}

function updateCategory(req, res) {
    var categoryId = req.params.id;

    var update = req.body;
    update.image = uploadImageCategory(req);

    Category.findByIdAndUpdate(categoryId, update, (err, category) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!category) {
                res.json(responseJson(404, "No se pudo actualizar la categoria", true, update));
            } else {
                res.json(responseJson(200, 'Categoria actualizada con exito', false, category));
            }
        }
    });

}

function deleteCategory(req, res) {
    //si se borra una categoria se borra todo lo que contenga esta como negocios, 
    //pero esto podria ser peligroso para la app
    var categoryId = req.params.id;

    Category.findByIdAndRemove(categoryId, (err, category) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!category) {
                res.json(responseJson(404, 'No se pudo eliminar la categoria', true, {}));
            } else {
                res.json(responseJson(200, 'Categoria eliminada con exito', false, category));
            }
        }
    });
}

function getImageCategory(req, res) {
    var imageFile = req.params.imageFile;
    var path_file = './upload/category/' + imageFile;
    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({
                message: 'No existe la imagen'
            });
        }
    });
}

module.exports = {
    saveCategory,
    getCategory,
    listCategories,
    updateCategory,
    deleteCategory,
    getImageCategory
}
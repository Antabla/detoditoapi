'use strict'

var Cryptr = require('cryptr');
var CRYPT_KEY = "90Fd6409B";
var cryptr = new Cryptr(CRYPT_KEY);
var moment = require('moment');

var Order = require('../models/order');

//<--------------------- PRIVATE FUNCTIONS ------------------------------>//

//This function response a Json object with a state code HTTP, a message, a error, a some data
function responseJson(code, message, error, data) {
    return {
        code: code,
        message: message,
        error: error,
        data: data
    };
}


//<--------------------- PUBLIC FUNCTIONS ------------------------------>//

function makeOrder(req, res) {
    var params = req.body;

    var order = new Order();

    order.bussines = params.bussines;
    order.date = moment().unix();
    order.state = 'false';
    order.details = params.details;
    order.shipping_price = params.shipping_price;
    order.total_order = params.total;

    order.save((err, orderStored) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {err}));
        } else {
            if (!orderStored) {
                res.json(responseJson(404, 'Error al hacer la orden', true, {}));
            } else {
                res.json(responseJson(200, 'Orden enviada', false, orderStored));
            }
        }
    });
}

//Accept or reject an order
function confirmateOrder(req, res) {
    var orderId = req.params.id;
    var confirmation = req.params.confirmation;

    if (confirmation) {
        Order.findById(orderId, (err, order) => {
            if (err) {
                res.json(responseJson(500, 'Error al recibir la peticion', true, {err}));
            } else {
                if (!order) {
                    res.json(responseJson(404, 'No se encontro la orden', true, {orderId}));
                } else {
                    order.state = true;

                    Order.findByIdAndUpdate(orderId, order, (err, orderUpdate) => {
                        if (err) {
                            res.json(responseJson(500,'Error al recibir la peticion',true,{err}));
                        } else {
                            if (!orderUpdate) {
                                res.json(responseJson(404,'No se encontro la orden',true,{orderId}));
                            } else {
                                res.json(responseJson(200,"Orden confirmada",false,order));
                            }
                        }
                    })
                }
            }
        })
    } else {    
        Order.findByIdAndRemove(orderId,(err, orderRemove) => {
            if (err) {
                res.json(responseJson(500,'Error al recibir la peticion',true,{err}));
            } else {
                if (!orderRemove) {
                    res.json(responseJson(404,'No se pudo eliminar la orden',true,{orderId}));
                } else {
                    res.json(responseJson(200,'Orden cancelada'));
                }
            }
        })
    }
}

//List the orders by business
function listOrders(req, res) {
    var page = req.params.page;
    var bussines = req.params.id;
    var itemsPerPage = 6;

    Order.find({ bussines: bussines }).sort('date').paginate(page, itemsPerPage, (err, order, total) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {err}));
        } else {
            if (!order) {
                res.json(responseJson(404, 'Error al obtener las ordenes', true, {bussines}));
            } else {
                res.json(responseJson(200, 'Ordenes obtenidas', false, { total: total, orders: order }));
            }
        }
    });

}

//Update a order getting it by id
function updateOrder(req, res) {
    var orderId = req.params.id;

    var update = req.body;

    Order.findByIdAndUpdate(orderId, update, (err, order) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {err}));
        } else {
            if (!order) {
                res.json(responseJson(404, "No se pudo actualizar la orden", true, update));
            } else {
                res.json(responseJson(200, 'Orden actualizada con exito', false, order));
            }
        }
    });

}

//Delete a order
function deleteOrder(req, res) {
    var orderId = req.params.id;

    Order.findByIdAndRemove(orderId, (err, order) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {err}));
        } else {
            if (!order) {
                res.json(responseJson(404, 'No se pudo eliminar la orden', true, {orderId}));
            } else {
                res.json(responseJson(200,'Orden eliminada',false,{order}));
            }
        }
    });
}

module.exports = {
    makeOrder,
    confirmateOrder,
    listOrders,
    updateOrder,
    deleteOrder
}
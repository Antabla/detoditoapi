'use strict'

var Cryptr = require('cryptr');
var CRYPT_KEY = "90Fd6409B";
var cryptr = new Cryptr(CRYPT_KEY);

var Bussines = require('../models/bussines');
var Type = require('../models/type');
var Product = require('../models/product');

//<--------------------- PRIVATE FUNCTIONS ------------------------------>//

//This function response a Json object with a state code HTTP, a message, a error, a some data
function responseJson(code, message, error, data) {
    return {
        code: code,
        message: message,
        error: error,
        data: data
    };
}

//this function returns the name of a image file
function uploadImageBussines(req) {
    var file_name = "null";

    if (req.files) {
        var file_path = req.files.image.path;
        var file_split = file_path.split("\\");

        file_name = file_split[2];
    }

    return file_name;
}

//<--------------------- PUBLIC FUNCTIONS ------------------------------>//

function saveBussines(req, res) {
    var params = req.body;

    var bussines = new Bussines();

    bussines.name = params.name;
    bussines.address = params.address;
    bussines.telephone = params.telephone;
    bussines.cellphone = params.cellphone;
    bussines.email = params.email;
    bussines.open = 'false';
    bussines.points = 0;
    bussines.image = uploadImageBussines(req);
    bussines.category = params.category;

    bussines.save((err, bussinesStored) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!bussinesStored) {
                res.json(responseJson(404, 'Error al guardar el negocio', true, {}));
            } else {
                res.json(responseJson(200, 'Negocio guardado', false, bussinesStored));
            }
        }
    });
}

//get a bussines by ID
function getBussines(req, res) {
    var bussinesId = req.params.id;

    var update = req.body;

    Bussines.findOneAndUpdate(bussinesId, update, (err, bussines) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!bussines) {
                res.json(responseJson(404, 'Error al actualizar el negocio', true, {}));
            } else {
                res.json(responseJson(200, 'Negocio actualizado', false, bussines));
            }
        }
    })
}

//get a bussines's by category and sort by points
function listBussines(req, res) {
    var page = req.params.page;
    var idCategory = req.params.id;
    var itemsPerPage = 6;

    Bussines.find({ category: idCategory }).sort('points').paginate(page, itemsPerPage, (err, bussines, total) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!bussines) {
                res.json(responseJson(404, 'Error al obtener los negocios', true, {}));
            } else {
                res.json(responseJson(200, 'Negocios obtenidos', false, { total: total, bussines: bussines }));
            }
        }
    });

}

//update a bussines getting it by id
function updateBussines(req, res) {
    var bussinesId = req.params.id;

    var update = req.body;
    update.image = uploadImageBussines(req);

    Bussines.findByIdAndUpdate(bussinesId, update, (err, bussines) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!bussines) {
                res.json(responseJson(404, "No se pudo actualizar el negocio", true, update));
            } else {
                res.json(responseJson(200, 'Negocio actualizado con exito', false, bussines));
            }
        }
    });

}

//delete a bussines with all your components by Id
function deleteBussines(req, res) {
    var bussinesId = req.params.id;

    Bussines.findByIdAndRemove(bussinesId, (err, bussines) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!bussines) {
                res.json(responseJson(404, 'No se pudo eliminar la categoria', true, {}));
            } else {
                Type.find({ bussines: bussines._id }).remove((err, type) => {
                    if (err) {
                        res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
                    } else {
                        if (!type) {
                            res.json(responseJson(404, 'No se pudo eliminar este tipo de producto', true, {}));
                        } else {
                            Product.find({ type: type._id }).remove((err, product) => {
                                if (err) {
                                    res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
                                } else {
                                    if (!type) {
                                        res.json(responseJson(404, 'No se pudo eliminar este producto', true, {}));
                                    } else {
                                        res.json(responseJson(200, 'Eliminado', false, bussines));
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

function getImageBussines(req, res) {
    var imageFile = req.params.imageFile;
    var path_file = './upload/bussines/' + imageFile;
    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({
                message: 'No existe la imagen'
            });
        }
    });
}

module.exports = {
    saveBussines,
    getBussines,
    listBussines,
    updateBussines,
    deleteBussines,
    getImageBussines
}
'use strict'

var Cryptr = require('cryptr');
var CRYPT_KEY = "90Fd6409B";
var cryptr = new Cryptr(CRYPT_KEY);

var Product = require('../models/product');

//<--------------------- PRIVATE FUNCTIONS ------------------------------>//

//This function response a Json object with a state code HTTP, a message, a error, a some data
function responseJson(code, message, error, data) {
    return {
        code: code,
        message: message,
        error: error,
        data: data
    };
}

//this function returns the name of a image file
function uploadImageProduct(req) {
    var file_name = "null";

    if (req.files) {
        var file_path = req.files.image.path;
        var file_split = file_path.split("\\");

        file_name = file_split[2];
    }

    return file_name;
}

//<--------------------- PUBLIC FUNCTIONS ------------------------------>//

function saveProduct(req, res) {
    var params = req.body;

    var product = new Product();

    product.name = params.name;
    product.image = uploadImageProduct(req);
    product.type = params.type;
    product.sale_price = params.sale_price;
    product.purchase_price = params.purchase_price;
    product.cellphone = params.cellphone;
    product.description = params.description;
    product.quantity = params.quantity;
    product.stock = params.stock;

    product.save((err, productStored) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!productStored) {
                res.json(responseJson(404, 'Error al guardar el producto', true, {}));
            } else {
                res.json(responseJson(200, 'Producto guardado', false, productStored));
            }
        }
    });
}

//get a product by ID
function getProduct(req, res) {
    var productId = req.params.id;

    var update = req.body;

    Product.findOneAndUpdate(productId, update, (err, product) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!product) {
                res.json(responseJson(404, 'Error al actualizar el producto', true, {}));
            } else {
                res.json(responseJson(200, 'Producto actualizado', false, product));
            }
        }
    })
}

//get a product's by type
function listProducts(req, res) {
    var page = req.params.page;
    var typeId = req.params.id;
    var itemsPerPage = 6;

    Product.find({ category: typeId }).paginate(page, itemsPerPage, (err, products, total) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!products) {
                res.json(responseJson(404, 'Error al obtener los productos', true, {}));
            } else {
                res.json(responseJson(200, 'Productos obtenidos', false, { total: total, products: products }));
            }
        }
    });

}

//update a product getting it by id
function updateProduct(req, res) {
    var productId = req.params.id;

    var update = req.body;
    update.image = uploadImageProduct(req);

    Product.findByIdAndUpdate(productId, update, (err, product) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!product) {
                res.json(responseJson(404, "No se pudo actualizar el producto", true, update));
            } else {
                res.json(responseJson(200, 'Producto actualizado con exito', false, product));
            }
        }
    });

}

//delete a product with all your components by Id
function deleteProduct(req, res) {
    var productId = req.params.id;

    Product.findByIdAndRemove(productId, (err, product) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!product) {
                res.json(responseJson(404, 'No se pudo eliminar la categoria', true, {}));
            } else {
                   res.json(responseJson(200,'Producto eliminado con exito',false,product));
            }
        }
    });
}

function getImageProduct(req, res) {
    var imageFile = req.params.imageFile;
    var path_file = './upload/product/' + imageFile;
    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({
                message: 'No existe la imagen'
            });
        }
    });
}

module.exports = {
    saveProduct,
    getProduct,
    listProducts,
    updateProduct,
    deleteProduct,
    getImageProduct
}
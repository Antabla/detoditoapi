
var Cryptr = require('cryptr');
var CRYPT_KEY = "90Fd6409B";
var cryptr = new Cryptr(CRYPT_KEY);

var Type = require('../models/type');
var Product = require('../models/product');

//<--------------------- PRIVATE FUNCTIONS ------------------------------>//

//This function response a Json object with a state code HTTP, a message, a error, a some data
function responseJson(code, message, error, data) {
    return {
        code: code,
        message: message,
        error: error,
        data: data
    };
}


//<--------------------- PUBLIC FUNCTIONS ------------------------------>//

//save a user
function saveType(req, res) {
    var params = req.body;

    var type = new Type();

    type.name = params.name;
    type.type = params.type;

    type.save((err, typeStored) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!type) {
                res.json(responseJson(404, 'No se pudo guardar el tipo', true, typeStored));
            } else {
                res.json(responseJson(200, 'tipo guardado', false, typeStored));
            }
        }
    });
}

function getType(req, res) {
    var typeId = req.params.type;

    Type.findById(typeId, (err, type) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (condition) {
                res.json(responseJson(404, 'No se encontro ninguel tipo', true, typeId));
            } else {
                res.json(responseJson(200, 'tipo encontrado', false, type));
            }
        }
    });
}

function listTypes(req, res) {
    var page = req.params.page;
    var itemsPerPage = 6;

    Type.find().paginate(page, itemsPerPage, (err, type, total) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!type) {
                res.json(responseJson(404, 'Error al obtener los tipos', true, {}));
            } else {
                res.json(responseJson(200, 'Tipos obtenidos', false, { total: total, type: type }));
            }
        }
    });

}

function updateType(req, res) {
    var typeId = req.params.id;

    var update = req.body;

    Type.findByIdAndUpdate(typeId, update, (err, type) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!type) {
                res.json(responseJson(404, "No se pudo actualizar el tipo", true, update));
            } else {
                res.json(responseJson(200, 'tipo actualizado con exito', false, type));
            }
        }
    });

}

function deleteType(req, res) {
    var typeId = req.params.id;

    Type.findByIdAndRemove(typeId, (err, type) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
        } else {
            if (!type) {
                res.json(responseJson(404, 'No se pudo eliminar el tipo', true, {}));
            } else {
                Product.find({ type: type._id }).remove((err, product) => {
                    if (err) {
                        res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
                    } else {
                        if (!product) {
                            res.json(responseJson(404, 'No se pudo eliminar este tipo', true, {}));
                        } else {
                            res.json(responseJson(200, 'Eliminado', false, product));

                        }
                    }
                });
            }
        }
    });
}


module.exports = {
    saveType,
    getType,
    listTypes,
    updateType,
    deleteType,
}
'use sctrict'


var Cryptr = require('cryptr');
var CRYPT_KEY = "90Fd6409B";
var cryptr = new Cryptr(CRYPT_KEY);

var moment = require('moment');

var jwt = require('../services/jwt');

var User = require('../models/user');


//<--------------------- PRIVATE FUNCTIONS ------------------------------>//

//This function response a Json object with a state code HTTP, a message, a error, a some data
function responseJson(code, message, error, data) {
    return {
        code: code,
        message: message,
        error: error,
        data: data
    };
}

//receives the data of a user and returns it coded
//HACERLO MAS GENERAL
function encodeUser(params, update = false) {

    var user = {};

    if (!update) {
        user = new User();
    }

    user.name = cryptr.encrypt(params.name);
    user.surname = cryptr.encrypt(params.surname);
    user.telephone = cryptr.encrypt(params.telephone);
    user.address = cryptr.encrypt(params.address);
    user.username = params.username.toLowerCase();
    user.email = cryptr.encrypt(params.email);
    user.password = cryptr.encrypt(params.password);
    user.role = cryptr.encrypt(params.role);
    user.favorites = [];
    user.orders = [];

    return user;
}

//receives a encodeuser and returns it decoded
function decodeUser(userEncode) {

    var userDecode = userEncode;

    userDecode.name = cryptr.decrypt(userEncode.name);
    userDecode.surname = cryptr.decrypt(userEncode.surname);
    userDecode.telephone = cryptr.decrypt(userEncode.telephone);
    userDecode.address = cryptr.decrypt(userEncode.address);
    userDecode.username = userEncode.username.toLowerCase();
    userDecode.email = cryptr.decrypt(userEncode.email);
    userDecode.password = cryptr.decrypt(userEncode.password);
    userDecode.role = cryptr.decrypt(userEncode.role);
    userDecode.favorites = userEncode.favorites;
    userDecode.orders = userEncode.orders;

    return userDecode;

}

//<--------------------- PUBLIC FUNCTIONS ------------------------------>//

//save a user
function saveUser(req, res) {

    var user = new User();
    var params = req.body;

    user = encodeUser(params);

    //yet no confirmation email
    user.activate = false;

    user.save((err, userStored) => {
        if (err) {
            if (err.code == '11000') {
                res.json(responseJson(200, 'Error ese nombre de usuario ya existe', true, { err }));
            } else {
                res.json(responseJson(500, 'Error al recibir la peticion', true, {}));
            }
        } else {
            if (!userStored) {
                res.json(responseJson(404, 'Error al guardar el usuario', true, {}));
            } else {
                var hashID = jwt.createToken(userStored._id);
                res.json(responseJson(200, 'Registrado con exito, porfavor revise su correo y confirme su cuenta', false, userStored));
            }
        }
    });

}

//get a user
function getUser(req, res) {
    
    var userId = req.params.id;

    User.findById(userId, (err, userFound) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, { err }));
        } else {
            if (!userFound) {
                res.json(responseJson(404, 'Error al obtener el usuario', true, {}));
            } else {
                var userFoundDecode = decodeUser(userFound);
                res.json(responseJson(200, 'Usuario encontrado', false, { user: userFoundDecode }));
            }
        }
    })
}

//login user
function loginUser(req, res) {
    var params = req.body;
    var username = params.username.toLowerCase();
    var password = params.password;

    if (username != null && password != null) {

        User.findOne({ username: username }).populate('favorites', 'orders').exec((err, user) => {
            
            if (err) {
                res.json(responseJson(500, 'Error al recibir la peticion', true, { err }));
            } else {
                if (!user) {
                    res.json(responseJson(404, 'No se encontro ningun usuario', true, {}));
                } else {
                    var passwordDeconde = cryptr.decrypt(user.password);

                    if (passwordDeconde == password) {

                        if (user.activate) {
                            user = decodeUser(user);
                            if (params.gethash) {
                                var token = jwt.createTokenUser(user);
                                res.json({ token });
                            } else {
                                res.json(responseJson(200, '', false, { user }))
                            }
                        } else {
                            res.json(responseJson(200, 'Esta cuenta no ha sido confirmada porfavor revise su correo', true, {}));
                        }
                    } else {
                        res.json(responseJson(200, 'La clave es erronea', true, {}));
                    }
                }
            }
        })
    } else {
        res.json(responseJson(200, "Porfavor rellene todos los campos", true, {}));
    }
}

//confirmation email
function confirmateEmail(req, res) {
    //mandar id y no el token
    var token = req.params.token;
    var id = jwt.decodeToken(token);

    User.findById(id, (err, user) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, { err }));
        } else {
            if (!user) {
                res.json(responseJson(404, 'No se pudo encontrar el usuario', true, {}));
            } else {
                if (!user.activate) {
                    user.activate = true;
                    User.findByIdAndUpdate(id, user, (err, userUpdate) => {
                        if (err) {
                            res.json(responseJson(500, 'Error al recibir la peticion', true, { err }));
                        } else {
                            if (!userUpdate) {
                                res.json(responseJson(404, 'Error al confirmar el correo', true, {}));
                            } else {
                                res.json(responseJson(200, 'Correo confirmado', false, {}));
                            }
                        }
                    });
                } else {
                    res.json(responseJson(200, 'Correo confirmado', false, {}));
                }
            }
        }
    });
}

function updateUser(req, res) {
    var userId = req.params.id;

    var update = encodeUser(req.body, true);

    User.findByIdAndUpdate(userId, update, (err, userUpdate) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, { err }));
        } else {
            if (!userUpdate) {
                res.json(responseJson(404, 'No se pudo actualizar el usuario', true, {}));
            } else {
                res.json(responseJson(200, 'Usuario actualizado', false, { user: userUpdate }));
            }
        }
    });
}

function removeUser(req, res) {
    var userId = req.params.id;

    User.findByIdAndRemove(userId, (err, userRemove) => {
        if (err) {
            res.json(responseJson(500, 'Error al recibir la peticion', true, { err }));
        } else {
            if (!userRemove) {
                res.json(responseJson(404, 'No se pudo eliminar el usuario', true, {}));
            } else {
                res.json(responseJson(200, 'Usuario Eliminado', false, { user: userRemove }));
            }
        }
    });

}

module.exports = {
    saveUser,
    getUser,
    loginUser,
    confirmateEmail,
    updateUser,
    removeUser
}
'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3977;

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/dsl', (err, res) => {
    if (err) {
        throw err;
    } else {
        console.log('la base de datos esta corriendo correctamente');
        app.listen(port, () => {
            console.log('Servidor del api rest esta corriendo en http://localhost:'+ port);
        });
    }
});

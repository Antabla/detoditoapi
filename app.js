//require librarys
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//Cargar rutas
var users_router = require('./routes/users');
var bussines_router = require('./routes/bussines');
var category_router = require('./routes/category');
var product_router = require('./routes/product');
var type_router = require('./routes/type');
var order_router = require('./routes/order');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//Archivos estaticos
app.use(express.static(path.join(__dirname, 'public')));

//Configurar cabeceras http
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API_KEY, Origin, X-Requested-With, Content-Type, Access, Access-Control-Allow-Request');
  res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
  res.header('Allow', 'GET, POST, DELETE, PUT, OPTIONS');
  
  next();
});

//Ruta base
app.use('/api', [users_router, bussines_router, category_router, product_router, type_router, order_router]);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

module.exports = app;

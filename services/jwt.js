'use strict'

var jwt = require('jwt-simple'); // para crear el token
var moment = require('moment'); // para tener la fecha de creacion y expiracion del token
var secret = 'De0226';


function createTokenUser(user) {
    var payload = {
        sub: user._id,
        name: user.name,
        surname: user.surname,
        telephone: user.telephone,
        adress: user.adress,
        username: user.username,
        email: user.email,
        password: user.password,
        role: user.role,
        activate: user.activate,
        favorites: user.favorites,
        orders: user.orders,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix
    };

    return jwt.encode(payload, secret);
}

function createToken(payload) {
    return jwt.encode(payload, secret);
}

function decodeToken(token) {
    return jwt.decode(token, secret);
}

module.exports = {
    createTokenUser,
    createToken,
    decodeToken
}
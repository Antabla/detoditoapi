'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TypeSchema = new Schema({
    name: String,
    bussines: {type: Schema.ObjectId, ref: 'Bussines'}
});

module.exports = mongoose.model("Type",TypeSchema);
'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = new Schema({
    bussines: {type: Schema.ObjectId, ref: 'Bussines'},
    date: { type: Date, default: Date.now },
    state: Boolean,
    details: [{
        product: {type: Schema.ObjectId, ref: 'Product'},
        quantity: Number,
        total: Number
    }],
    shipping_price: Number,
    total_order: Number
});

module.exports = mongoose.model("Order",OrderSchema);
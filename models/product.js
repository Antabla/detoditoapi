'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
    name: String,
    image: String,
    type: {type: Schema.ObjectId, ref: 'Type'},
    sale_price: Number,
    purchase_price: Number,
    description: String,
    quantity: Number,
    stock: Number,
});

module.exports = mongoose.model("Product",ProductSchema);
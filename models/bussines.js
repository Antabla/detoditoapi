'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BussinesSchema = new Schema({
    name: String,
    address: String,
    telephone: String,
    cellphone: String,
    email: String,
    open: Boolean,
    points: Number,
    image: String,
    category: { type: Schema.ObjectId, ref: 'Category' }
    
});

module.exports = mongoose.model("Bussines", BussinesSchema);
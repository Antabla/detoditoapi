'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: String,
    surname: String,
    telephone: String,
    address: String,
    username: {type: String, unique: true, required: true},
    email:String,
    password: String,
    favorites: [{type: Schema.ObjectId, ref: 'Bussines'}],
    orders: [{type: Schema.ObjectId, ref: 'Order'}],
    role: String,
    activate: Boolean
    ///time to delete
});

module.exports = mongoose.model('User', userSchema);